# Welcome

Welcome to the website for the {{ psm_long }}, maintained by the Edmond ACM Chapter.

## About

The NASA Center of Excellence for Collaborative Innovation (CoECI) through an
Interagency Agreement with the Centers for Medicare and Medicaid Services (CMS)
administered a crowd-sourced application development challenge for the Medicare
and Medicaid Services, Center for Medicaid and CHIP Services (CMCS). The
challenge was to build a multi-state, multi-program provider screening
application capable of risk scoring, credentialing validation, identity
authentication, and sanction checks, that lowered the burden on providers and
reduced administrative and infrastructure expenses for states and federal
programs.

The application was built using NASA's contract with Harvard Business School in
association with the Institute of Quantitative Social Sciences and their
subcontract with TopCoder.  The Application Development Challenge was sponsored
by CMS as part of the Partnership for Program Integrity Innovation program with
the specific intent of developing an application to improve capabilities for
streamlining operations and screening providers to reduce fraud and abuse.

The challenge was comprised of an omnibus of 120 contests launched between June
2012 and April 2013, to cover all phases of the software development life cycle.
The code and documentation resulting from that challenge are hosted in an
[original repository](https://github.com/NASA-Tournament-Lab/coeci-cms-mpsp)
code, and in 2017 [another repository](https://github.com/SolutionGuidance/psm)
was created to continue the work.

Unfortunately, in late 2018 the project was abandoned and work ceased, the
domain name expired, and the project was all but forgotten until late 2020 when
the project was picked up by EMR Technical Solutions in Edmond, OK and
subsequently transferred to the Edmond ACM Chapter.

## Contributions

The maintained version of the {{ psm_long }} that relates to this website is
hosted on [Bitbucket](https://bitbucket.org/acmcentral/psm). The
Edmond ACM Chapter is still in the process of cleaning up the source code and
ensuring that everything is ready for public contribution.

This website is open source as well! If you have any corrections or suggestions,
we invite you to [contribute](https://bitbucket.org/acmcentral/psm-public-website).

## Proprietary Services

Since this project has been released under the Apache 2.0 license, companies are
more than welcome to build proprietary modifications and provide dedicated
support services. Some of these services are noted below.

- [PSM+](https://mmis.cloud) - developed and maintained by EMR Technical Solutions
