<?php

/**
 * Standard Replacement File
 *
 * Any key placed in this array will be automatically replaced with the value should
 * it be found within a page's content.
 *
 * The difference between these are custom variables are that:
 * - Custom variables have to be manually inputted into a view, whereas standard
 *   replacements will be parsed automatically if the matching text is found.
 * - Standard replacements only apply at the page level, not the view level.
 * 
 * @author      j-belelieu
 * @date        7/4/15
 * @package     Banana Dance Lite
 * @link        http://www.bananadance.org/
 * @link        http://www.bananadance.org/docs/?l=named_routes
 * @license     GPL-3.0
 * @link        http://www.opensource.org/licenses/gpl-3.0.html
 */

return array(

    'en' => array(
        'ACM Central' => '<a href="https://acmcentral.org">ACM Central</a>',
        'Edmond ACM Chapter' => '<a href="https://acmcentral.org">Edmond ACM Chapter</a>',
        'EMRTS' => '<a href="https://emrts.us" target="_blank">EMRTS</a>',
        'EMR Technical Solutions, LLC' => '<a href="https://emrts.us" target="_blank">EMR Technical Solutions, LLC</a>',
        'EMR Technical Solutions' => '<a href="https://emrts.us" target="_blank">EMR Technical Solutions</a>',
        'jbelelieu' => '<a href="http://twitter.com/jbelelieu" target="_blank">@jbelelieu</a>',
    ),

    //'fr' => array(
    //    'jbelelieu' => '<a href="http://twitter.com/jbelelieu" target="_blank">@jbelelieu</a>',
    //),

);