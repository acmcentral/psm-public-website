# psm-public-website

This is the public-facing website for the [Provider Screening Module](https://projectpsm.org)
and is currently maintained by [EMR Technical Solutions](https://emrts.us).

The website itself was built on top of [Banana Dance Lite](https://github.com/jbelelieu/banana-dance-lite).
